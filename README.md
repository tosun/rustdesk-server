# RustDesk服务器程序

[**下载**](https://gitee.com/rustdesk/rustdesk-server/releases)

[**教程**](https://rustdesk.com/blog/id-relay-set/)

[**需要注册邮箱才能运行**](https://rustdesk.com/zh/server/)

借用此地发布RustDesk服务器程序和跟踪问题。

如果您需要一个Windows客户端安装无需手动填写自定义服务器地址的方案，也请[电邮](mailto:info@rustdesk.com)联系，我们已经有了完整解决方案。

# [电邮](mailto:info@rustdesk.com)请求3天免费测试