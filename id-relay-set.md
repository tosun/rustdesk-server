# 如何自建中继:
-----------

## 下载服务器端软件程序

[下载](https://gitee.com/rustdesk/rustdesk-server/)或者使用docker rustdesk/rustdesk-server，**注意**： 你需要[购买许可](https://rustdesk.com/server/)才能正常运行本程序

提供三个版本：
  - Linux
  - Windows
  - 群晖套件，此版本是在Linux上打包而成，运行log分别是/var/log/hbbs.log和/var/log/hbbr.log，建议安装[LogAnalysis套件](https://www.cphub.net)查看，请忽略C++版本报错信息，如果运行正常。

以下针对Linux版本做使用说明。

有两个可执行文件:
  - hbbs - RustDesk ID注册服务器
  - hbbr - RustDesk 中继服务器

下载后务必
```
chmod a+x hbbs hbbr
```

Linux版本在Centos7构建，在 Centos7/8，Ubuntu 18/20上测试过，Debian系列的发行版本应该都没有问题。如果有其他发行版本需求，请联系我。

### 服务器要求
硬件要求很低，最低配置的云服务器就可以了，CPU和内存要求都是最小的。关于网络大小，如果TCP打洞直连失败，就要耗费中继流量，一个中继连接的流量在30k-3M每秒之间（1920x1080屏幕），取决于清晰度设置和画面变化，如果只是办公需求，平均在100K。

### 在服务器上运行 hbbs 和 hbbr

在服务器上运行 hbbs/hbbr (Centos 或 Ubuntu)。建议使用[pm2](https://pm2.keymetrics.io/) 管理服务，这里有[Windows下pm2安装RustDesk教程](https://gitee.com/rustdesk/rustdesk-server/blob/master/pm2-windows.md)。

需要先运行 hbbr, 可以不带任何参数;
然后运行 hbbs:
```
./hbbs -r <hbbr运行所在主机的地址>
```
hhbs的-r参数不是必须的，他只是方便你不用在客户端指定中继服务器。客户端指定的中继服务器优先级高于这个。

默认情况下，hbbs 监听21115(tcp)和21116(tcp/udp) ，hbbr 监听21117(tcp)。务必在防火墙开启这几个端口， **请注意21116同时要开启TCP和UDP** ，请注意21116要同时开启TCP和UDP。其中21115是hbbs用作NAT类型测试，21116/UDP是hbbs用作ID注册与心跳服务，21116/TCP是hbbs用作TCP打洞与连接服务，21117是hbbr用作中继服务。

- TCP(21115, 21116, 21117)
- UDP(21116)

如果你想选择**自己的端口**，使用 “-h” 选项查看帮助。

#### Docker示范
```
sudo docker image pull rustdesk/rustdesk-server
sudo docker run --name hbbr -p 21117:21117 -v `pwd`:/root -it --rm rustdesk/rustdesk-server hbbr -m <registered_email>
sudo docker run --name hbbs -p 21115:21115 -p 21116:21116 -p 21116:21116/udp -v `pwd`:/root -it --rm rustdesk/rustdesk-server hbbs -r <relay-server-ip> -m <registered_email>
```

### 在客户端设置 hbbs/hbbr 地址

点击 ID 右侧的菜单按钮如下，选择“ ID/中继服务器”。

![image](https://user-images.githubusercontent.com/71636191/113117333-e73c8f00-9240-11eb-8653-fc0c2ae4f0bf.png)

在 ID 服务器输入框中输入 hbbs 主机或 ip 地址，在中继服务器输入框中输入 hbbr 主机或 ip 地址。

**请注意**图中的Key不是指的注册邮箱，对应的是hhbr/hhbs的-k参数，这是为了防止别人盗用你的hbbr/hbbs使用。

例如:

```
hbbs.yourhost.com
hbbr.yourhost.com
```

或者

```
hbbs.yourhost.com:21116
hbbr.yourhost.com:21117
```
![输入图片说明](https://images.gitee.com/uploads/images/2021/0511/154228_4b23c5b3_8131681.png "屏幕截图.png")
