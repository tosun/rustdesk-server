### 安装NodeJs
[下载](https://nodejs.org/dist/v14.16.1/node-v14.16.1-x64.msi)安装，可能会有点慢，如果卡顿太久，尝试关掉重新安装。

### 安装pm2
在cmd.exe中分别输入下面三行，每一行都要按回车键，一行一行运行。
```
npm install -g pm2
npm install pm2-windows-startup -g
pm2-startup install
```

### 运行hbbr和hbbs
下载[服务器程序](https://gitee.com/rustdesk/rustdesk-server/attach_files/691620/download/rustdesk-server-windows-x64.zip)，假设你解压缩到了C盘下。分别运行下面四行命令。
```
cd c:\rustdesk-server-windows-x64
pm2 start hbbr.exe -- -m 注册邮箱地址
pm2 start hbbs.exe -- -r hbbr运行所在主机的地址 -m 注册邮箱地址
pm2 save
```

### 查看log
```
pm2 log hbbr
pm2 log hbbs
```